package Flowers;

import java.util.Comparator;

public class MultiplyComparator implements Comparator<Flower> {

    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getMultiplying() - o2.getMultiplying();
    }
}
