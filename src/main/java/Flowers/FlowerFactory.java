package Flowers;

public class FlowerFactory {

    public static void setValues(Flower flower) {
        ValueParser parser = new ValueParser(flower.getType(),"src/main/resources/values.xml");

        flower.setName(parser.parametersMap().get("name").toString());
        flower.setSoil(parser.parametersMap().get("soil").toString());
        flower.setOrigin(parser.parametersMap().get("origin").toString());
        flower.setMultiplying(Integer.parseInt(parser.parametersMap().get("multiplying").toString()));
    }
}
