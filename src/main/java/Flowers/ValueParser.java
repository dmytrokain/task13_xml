package Flowers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ValueParser {
    private File file;
    private Document doc;
    String [] parametersEnum = {"name", "soil", "origin", "multiplying"};

    HashMap<String, String> parameters;

    public ValueParser(String flowerType, String xmlPath)  {

        try {
            file = new File(xmlPath);
            parameters = new HashMap<>();

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.parse(file);
            doc.getDocumentElement().normalize();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        parseXml(flowerType);
    }

    private void parseXml(String flowerType) {
        NodeList nodeList = doc.getElementsByTagName("Flower");

        for(int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                if(element.getAttribute("type").equals(flowerType)) {
                    for(String parameter: parametersEnum) {
                        parameters.put(parameter, element.getElementsByTagName(parameter)
                                .item(0)
                                .getTextContent());
                    }
                }
            }
        }
    }

    public HashMap parametersMap() {
        return parameters;
    }


    public static void main(String[] args) {
        ValueParser valueParser = new ValueParser("2","src/main/resources/values.xml");

        System.out.println(valueParser.parametersMap());
    }
}
