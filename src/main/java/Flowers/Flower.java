package Flowers;

import java.util.Comparator;

public class Flower implements Comparable<Flower> {

    private String type;
    private String name;
    private String soil;
    private String origin;
    private int multiplying;

    public Flower() {

    }

    public Flower(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMultiplying(int multiplying) {
        this.multiplying = multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getType() {
        return type;
    }

    public int getMultiplying() {
        return multiplying;
    }

    @Override
    public int compareTo(Flower o) {
        return Integer.valueOf(this.getMultiplying()).compareTo(o.getMultiplying());
    }

    @Override
    public String toString() {
        return "Info:"
                + "\n----"
                + "\nname: " + getName()
                + "\nsoil: " + getSoil()
                + "\norigin: " + getOrigin()
                + "\nmultiplying: " + getMultiplying()
                + "\n----";
    }


}
