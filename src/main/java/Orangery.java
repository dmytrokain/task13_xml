import Flowers.Flower;
import Flowers.FlowerFactory;

import java.util.*;

public class Orangery extends TreeSet<Flower> {

    public static void main(String[] args) {

        Flower flower = new Flower("Romashka");
        Flower flower2 = new Flower("Barvinok");
        FlowerFactory.setValues(flower);
        FlowerFactory.setValues(flower2);

        Orangery orangery = new Orangery();
        orangery.add(flower);
        orangery.add(flower2);

        System.out.println(orangery);
    }
}